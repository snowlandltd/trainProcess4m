clc
clear
close all

x1 = -5 + rand(200,2)*3 + 10;
x2 = [rand(100,1)+2 ,rand(100,1)-4] + 10;
x3 = [rand(100,1)*2+0 ,rand(100,1)*2-2] + 10;
y = [ones(1,200) , ones(1,100)*2 ,ones(1,100)*3];

r = randperm(400);

x = [x1;x2;x3];
x = x(r,:);
y = y(r);

figure(1)
hold on
plot(x1(:,1),x1(:,2),'b.')
plot(x2(:,1),x2(:,2),'r.')
plot(x3(:,1),x3(:,2),'g.')
[nx,ny] = trnDataProcess(x,y,0.7);

% figure(2)
hold on

plot(nx(ny==1,1),nx(ny==1,2),'bo')
plot(nx(ny==2,1),nx(ny==2,2),'ro')
plot(nx(ny==3,1),nx(ny==3,2),'go')