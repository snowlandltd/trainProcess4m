function d = dist_2(A,b,a)
% http://www.docin.com/p-1746417622.html
m = size(a,1);
d = zeros(m,1);
for i = 1 : m
    d(i) = norm(A'*((A*A')\(A*a(i,:)'-b)));
end
end