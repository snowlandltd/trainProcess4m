function [newTrnData,newClasses] = trnDataProcess(trnData,classes,percent)
T_class = unique(classes);
len = length(T_class);

pop_num = size(trnData,1);
newTrnData_index = false(1, pop_num);

dist_map = []; % 行方向是数据点;列方向是超平面个数，与lebel_map长度相等
label_map = []; %
% 对每一类循环
for i = 1:len
    select_index = classes==T_class(i);
    data_class = trnData(select_index,:);

    K = convhulln(data_class);
    [mK, nK] = size(K);
    
    %   对每一超平面循环
    for ik = 1 : mK
        y = null([data_class(K(ik,:),:),-ones(nK,1)],'r');
        plane_A = y(1:end-1,1)';
        plane_b = y(end,1);
        dist_map = [dist_map;dist_2(plane_A,plane_b,trnData)'];
    end
    label_map = [label_map, ones(1, mK) * T_class(i)];
end

[m_dist_map, ~] = size(dist_map);

% 区分分类间与类外的距离
dist_inner_map = zeros(1,pop_num);
dist_outer_map = zeros(1,pop_num);
for i = 1 : m_dist_map
    select = classes == classes(i);
    dist_inner_map(i) = min(dist_map(i, select));
    dist_outer_map(i) = min(dist_map(i, ~select));
    
end

P_inner = zeros(1,pop_num);
P_outer = zeros(1,pop_num);
for i = 1 : len
    
    select_index = classes==T_class(i);
    
    dist_inner = dist_inner_map(select_index);
    mu_inner = 0;
    sigma_inner = ((max(dist_inner) - mu_inner)/2);
    P_inner_eachclass = (1-normcdf(dist_inner,mu_inner,sigma_inner))*2;
    
    dist_outer = dist_outer_map(select_index);
    mu_outer = 0;
    sigma_outer = ((max(dist_outer) - mu_outer)/2); %%%%%%%%%%%% 2 可调
    P_outer_eachclass = (1-normcdf(dist_outer,mu_outer,sigma_outer))*2;
    
    P_inner(select_index) = P_inner_eachclass;
    P_outer(select_index) = P_outer_eachclass;
end

P = 0.5 * P_inner + 0.5 * P_outer; %%%%%%%%%%%%% 权值可调
% 选中点个数
select_pop = ceil(pop_num * percent);
newTrnData_index = zeros(1,select_pop);
plist = 1 : pop_num;
for i = 1 : select_pop
    P_cumsum = [0,cumsum(P(plist))];

    mrand = rand() * P_cumsum(end);
    ind = find(P_cumsum <= mrand, 1, 'last' );
    
    newTrnData_index(i) = plist(ind);
    plist(ind) = [];
end

newTrnData = trnData(newTrnData_index,:);
newClasses = classes(newTrnData_index);
end